import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';


const endpoint = 'http://localhost:8080/TimeService/rest/';
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

@Injectable({
    providedIn: 'root'
})
  
export class TimeRestService {

    
  constructor(private http: HttpClient) {


  }
  
  public getTime(hour, timezone): Observable<any> {

    console.log(hour+"/"+timezone);
    return this.http.post<any>(endpoint + 'time/'+hour+'/'+timezone, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('getTime'))
    );
   
  }
  

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      alert(error.message);
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  

}
