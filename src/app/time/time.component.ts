import { Component, OnInit } from '@angular/core';
import { TimeRestService } from '../time-rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import {ClientService} from '../client.service';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {

  time:any = [];

  constructor(public rest:TimeRestService, private route: ActivatedRoute, private router: Router, private client: ClientService) { }

  ngOnInit() {
    this.time  = JSON.parse(this.client.getUtc());

    //this.getTime("10:22:33","-3");
  }

  getTime(time, timezone) {
    this.time = [];
    this.rest.getTime(time, timezone).subscribe((data: {}) => {
      console.log("data "+data);
      this.time = data;
    });
  }

}
