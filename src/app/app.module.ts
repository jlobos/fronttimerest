import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { TimeComponent } from './time/time.component';
import { TimeFormComponent } from './timeform/time-form.component'
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  {
    path: 'time',
    component: TimeComponent,
    data: { title: 'Get Time' }
  },
  {
    path: '',
    component: TimeFormComponent,
    data: { title: 'Get Time' }
  }
  
  ];

@NgModule({
  declarations: [
    AppComponent,
    TimeComponent,
    TimeFormComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
	ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
