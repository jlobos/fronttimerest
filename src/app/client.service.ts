import {Injectable} from '@angular/core';

const UTC = '';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  setUtc(utc: string): void {
    localStorage.setItem(UTC, utc);
  }

  getUtc(){
      return localStorage.getItem(UTC);
  }
}