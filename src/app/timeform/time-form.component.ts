import { Component, Input, NgZone, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {TimeRestService} from '../time-rest.service';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ClientService} from '../client.service';

@Component({
  selector: 'app-time-form',
  templateUrl: './time-form.component.html',
  styleUrls: ['./time-form.component.css']
})
export class TimeFormComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;


  ngOnInit() { //Cambiar
    this.registerForm = this.formBuilder.group({
      time: ['', [Validators.required]],
      timezone: ['', [Validators.required]]
  });
  }
  

  constructor(private formBuilder: FormBuilder, private api: TimeRestService, private client: ClientService, private router: Router, private zone:NgZone) {
  }

  get f() { return this.registerForm.controls; }
  timeutc:any = [];
  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.time();
  }

  time() {
    this.timeutc = [];
    this.api.getTime( this.registerForm.get('time').value,
    this.registerForm.get('timezone').value).subscribe((data: {}) => {

      if(data){
        this.client.setUtc(JSON.stringify(data));
        this.router.navigateByUrl('/time');
      }
    });
     /* this.api.getTime(
      this.registerForm.get('time').value,
      this.registerForm.get('timezone').value
    ).subscribe(
        r => {
          if(r){
            
            this.router.navigateByUrl('/time');
        }
        },
        r => {
          alert(r.error.error);
        });*/
  }

}
